from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLineEdit, QTabWidget, QScrollArea
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt
from flowlayout import FlowLayout
from emojibutton import EmojiButton
from collections import OrderedDict
import util, os

class EmojiKeyboard(QWidget):
    _closing = False

    def __init__(self):
        super().__init__()

        vbox = QVBoxLayout()
        self.setLayout(vbox)

        search_bar = QLineEdit(self)
        vbox.addWidget(search_bar)

        tabs = QTabWidget()
        vbox.addWidget(tabs)
        for category, data in util.emojidata["categories"].items():
            scroll = QScrollArea()

            flow = FlowLayout()
            flow_widget = QWidget()
            flow_widget.setLayout(flow)

            scroll.setWidget(flow_widget)
            scroll.setWidgetResizable(True)

            for emoji in data.values():
                button = EmojiButton(emoji)
                flow.addWidget(button)

            path = os.path.join(os.path.dirname(__file__), "res/emoji/png_512/" + list(data.values())[0].get("unicode") + ".png")
            tabs.addTab(scroll, QIcon(path), category)

    def quit(self):
        self._closing = True
        self.close()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()

    def closeEvent(self, event):
        if self._closing:
            event.accept()
        else:
            event.ignore()
            self.hide()

