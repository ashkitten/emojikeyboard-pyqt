from PyQt5.QtWidgets import QDialog, QGridLayout, QLabel, QSizePolicy, QPushButton, QApplication
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
import os

class EmojiPopup(QDialog):
    def __init__(self, emoji):
        super().__init__()

        self.emoji = emoji

        grid = QGridLayout()

        emoji_name = QLabel(self.emoji.get("name").title())
        emoji_name.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        grid.addWidget(emoji_name, 0, 0)

        emoji_unicode = QLabel("U+" + self.emoji.get("unicode"))
        grid.addWidget(emoji_unicode, 1, 0)

        copy_button = QPushButton("Copy")
        copy_button.clicked.connect(self.on_copy_button_clicked)
        grid.addWidget(copy_button, 0, 1, 2, 1)

        path = os.path.join(os.path.dirname(__file__), "res/emoji/png_512/" + emoji.get("unicode") + ".png")
        pixmap = QPixmap(os.path.join(os.path.dirname(__file__), path))
        image_label = QLabel()
        image_label.setPixmap(pixmap)
        grid.addWidget(image_label, 2, 0, 1, 2)

        self.setLayout(grid)

        self.setAttribute(Qt.WA_DeleteOnClose)

    def on_copy_button_clicked(self):
        clipboard = QApplication.clipboard()
        clipboard.setText(self.emoji.get("characters"))
