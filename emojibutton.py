from PyQt5.QtWidgets import QPushButton
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize
from emojipopup import EmojiPopup
import os

class EmojiButton(QPushButton):
    def __init__(self, emoji):
        super().__init__()

        self.emoji = emoji

        path = os.path.join(os.path.dirname(__file__), "res/emoji/png_64/" + emoji.get("unicode") + ".png")
        self.setIcon(QIcon(path))
        self.setIconSize(QSize(24, 24))

        self.clicked.connect(self.on_clicked)

    def on_clicked(self):
        win = EmojiPopup(self.emoji)
        win.exec()
