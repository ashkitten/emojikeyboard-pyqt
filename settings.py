from emojicontainer import EmojiContainer
import util

class Settings:
    settings = { "favorites": [] }
    favorites_page = EmojiContainer()

    def add_favorite(emoji):
        Settings.settings["favorites"].append(emoji["unicode"])
        Settings.favorites_page.add_emoji(emoji)

    def remove_favorite(emoji):
        Settings.settings["favorites"].remove(emoji["unicode"])
        Settings.favorites_page.remove_emoji(emoji)

    def is_favorite(emoji):
        if emoji["unicode"] in Settings.settings["favorites"]:
            return True
        return False

    def load_settings():
        if os.path.isfile(os.path.join(os.path.dirname(__file__), "settings.json")):
            with open(os.path.join(os.path.dirname(__file__), "settings.json"), encoding="utf-8") as f:
                Settings.settings = json.load(f)
                for emoji in Settings.settings["favorites"]:
                    Settings.favorites_page.add_emoji()

    def save_settings():
        with open(os.path.join(os.path.dirname(__file__), "settings.json"), "w+", encoding="utf-8") as f:
            json.dump(Settings.settings, f)
