from collections import OrderedDict
import json, os

emojidata = json.load(open(os.path.join(os.path.dirname(__file__),"res/emoji.json"), encoding="utf-8"), object_pairs_hook=OrderedDict)
