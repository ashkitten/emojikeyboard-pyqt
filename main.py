from PyQt5.QtWidgets import QApplication, QSystemTrayIcon, QMenu
from PyQt5.QtGui import QIcon
from emojikeyboard import EmojiKeyboard
import sys, os

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setApplicationName("EmojiKeyboard")

    win = EmojiKeyboard()
    win.resize(500, 300)
    win.setWindowTitle("Emoji Keyboard")
    win.show()

    if QSystemTrayIcon.isSystemTrayAvailable():
        menu = QMenu()

        show_action = menu.addAction("Show")
        show_action.triggered.connect(win.show)

        quit_action = menu.addAction("Quit")
        quit_action.triggered.connect(win.quit)

        def on_tray_icon_activated(reason):
            if reason == QSystemTrayIcon.Trigger:
                win.show()

        tray_icon = QSystemTrayIcon(QIcon(os.path.join(os.path.dirname(__file__), "res/icon.png")))
        tray_icon.setContextMenu(menu)
        tray_icon.setToolTip("Emoji Keyboard")
        tray_icon.show()

        tray_icon.activated.connect(on_tray_icon_activated)

    sys.exit(app.exec_())
